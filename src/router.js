import Vue from 'vue'
import Router from 'vue-router'
import DashboardLayout from '@/layout/DashboardLayout'
import AuthLayout from '@/layout/AuthLayout'
import { hasAccessToken } from './api'

Vue.use(Router)

const router = new Router({
  linkExactActiveClass: 'active',
  routes: [
    {
      path: '/',
      redirect: 'dashboard',
      component: DashboardLayout,
      children: [
        {
          path: '/dashboard',
          name: 'dashboard',
          // route level code-splitting
          // this generates a separate chunk (about.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          component: () => import(/* webpackChunkName: "demo" */ './views/Dashboard.vue'),
          meta: { requiresAuth: true }
        }
      ]
    },
    {
      path: '/',
      component: AuthLayout,
      children: [
        {
          path: '/login',
          name: 'login',
          component: () => import(/* webpackChunkName: "demo" */ './views/Login.vue'),
          meta: { isLogin: true }
        },
        {
          path: '/token/:token',
          name: 'token',
          component: () => import(/* webpackChunkName: "demo" */ './views/Token.vue'),
          meta: { isLogin: true },
          props: true,
        }
      ]
    }
  ]
})

router.beforeEach(async (to, from, next) => {
  console.log('to ' + to.name)

  const isLogin = to.matched.some(x => x.meta?.isLogin)
  console.log('isLogin=' + isLogin)
  if (isLogin) {
    next();
    return;
  }

  const requiresAuth = to.matched.some(x => x.meta?.requiresAuth)
  if (requiresAuth && !hasAccessToken()) {    
    next('/login')
  } else {
    next()
  }
})

export default router;
