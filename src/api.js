import axios from "axios";

const AUTH_URL = "https://svjukrajinska14.cz/api/auth/v1"
const PUBLIC_URL = "https://svjukrajinska14.cz/api/public/v1"

let ACCESS_TOKEN;

export function hasAccessToken() {
  return !!ACCESS_TOKEN;
}

export function apiSetAccessToken(accessToken) {
  ACCESS_TOKEN = accessToken;
}

export async function apiStartOtp(emailAddress, unitNo) {
  unitNo = typeof unitNo === 'number' ? unitNo : parseInt(unitNo, 10);
  const response = await axios.post(`${AUTH_URL}/otp-start`, {
    emailAddress,
    unitNo,
  }, { validateStatus: false });

  return response.status >= 200 && response.status < 300;
}

export async function apiFinishOtp(token) {
  const response = await axios.post(`${AUTH_URL}/otp-finish`, {
    token
  }, { validateStatus: false });

  if (response.status === 200 && response.data.accessToken) {
    return response.data.accessToken;
  }

  return undefined;
}

export async function apiGetStats() {
  const response = await axios.get(`${PUBLIC_URL}/stats`, {
    headers: {
      'authorization': `bearer ${ACCESS_TOKEN}`
    }
  }, { validateStatus: false });

  if (response.status === 200) {
    return response.data;
  }
  return undefined;
}
